swh.core[http] >= 0.3  # [http] is required by swh.core.pytest_plugin
swh.lister >= 2.9.0
swh.loader.core >= 3.1.0
swh.storage >= 0.29.0
